package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/DeleteTip")
public interface DeleteTip {

	@DELETE
	@Path("/deleteTip")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteTip(@QueryParam("tip_id") String tip_id);
}
