/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/PostVideo")
public class UpdateVideo {

	@POST
	@Path("/postVideo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateVideo(String json) {
		return null;
	}
}
