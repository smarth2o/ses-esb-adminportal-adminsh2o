/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/MeterConsumptionData")
public class MeterConsumptionData {

	@GET
	@Path("/getMeterConsumption")
	@Produces(MediaType.APPLICATION_JSON)
	public String getMeterConsumptionData(@QueryParam("meter_id") String meter_id) {
		return null;
	}
}
