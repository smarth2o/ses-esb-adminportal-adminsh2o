/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/DeleteVideo")
public class DeleteVideo {

	@DELETE
	@Path("/deleteVideo")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteVideo(@QueryParam("video_id") String video_id) {
		return null;
	}
}
