/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/DeleteTip")
public class DeleteTip {

	@DELETE
	@Path("/deleteTip")
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteTip(@QueryParam("tip_id") String tip_id) {
		return null;
	}
}
