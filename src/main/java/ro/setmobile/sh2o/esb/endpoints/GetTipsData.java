/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/GetTips")
public class GetTipsData {

	@GET
	@Path("/getTips")
	@Produces(MediaType.APPLICATION_JSON)
	public String getTips(@QueryParam("lang") String lang) {
		return null;
	}
}
