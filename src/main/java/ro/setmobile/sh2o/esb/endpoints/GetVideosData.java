/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewAdminSH2O/GetVideosData")
public class GetVideosData {

	@GET
	@Path("/getVideos")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVideos() {
		return null;
	}
}
